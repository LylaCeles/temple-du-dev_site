const express = require('express') // va importer express
const app = express() // on instancie un serveur

const port = 3000 // port du serv
const path = require("path");

app.use(express.static(path.join(__dirname, '/public')))
app.use(express.json());


// route pour serv App => / : affiche avec le send("blbl")
app.get('/', (req, res) => {
    socket.connect();
    socket.emit("chat message", "voila un boooooo message");
    socket.on("chat message", (msg)=>{
        console.log(msg); 
    });   
    res.send('<a href="/blbl">Hello World!</a> <a href="/product">go to product json </a>')
})

app.get('/blbl', (req, res) => {
    // req => request, les données qu'on envoie
    // res => responce, les données qu'on recois
    res.send('<a href="/">go to index </a> <a href="/product">go to product json </a> Hey, it\' the blbl page !')
})

app.post('/products', (req, res) => {
    // on utilise postman pour envoyé des request

    if(req.body.blbl == "coucou"){
        res.send("Hey !");
    }
    else{
        // res.send("owh, no more coucou for me :(")
        res.status(403).end()
    }
})

app.get("/index", (req, res)=>{
    res.sendFile(path.join(__dirname+'/src/index.html'));


})

app.route("/contact")
    .all(function(res,req, next){
        if(true){
            next();
        }
        res.sendFile(path.join(__dirname+'/src/index.html'));

    })

    .get(function(res,req){
        // affiche contacte
    })

    .post(function(res,req){
        // ajoute un contacte
        res.status(404).end()
    })


// le serv va écouté sur le port 3000



// on doit installer nodemon afin de pouvoir avoir un refresh automatique !
// on ajoute dans package.json : "start": "nodemon index.js" !
// a la place de faire node index.js pour lancé cette ligne, on fait npm run start