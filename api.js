const express = require('express') // va importer express
const app = express() // on instancie un serveur
const cors = require("cors");
const port = 3000 // port du serv
const path = require("path");
app.use(express.static(path.join(__dirname, '/public')))
app.use(express.json());
app.use(cors());


app.get('/blbl', (req, res) => {
    // req => request, les données qu'on envoie
    // res => responce, les données qu'on recois
    res.send('<a href="/">go to index </a> <a href="/product">go to product json </a> Hey, it\' the blbl page !')
})

app.post('/products', (req, res) => {
    // on utilise postman pour envoyé des request

    if(req.body.blbl == "coucou"){
        res.send("Hey !");
    }
    else{
        // res.send("owh, no more coucou for me :(")
        res.status(403).end()
    }
})

app.get("/index", (req, res)=>{
    res.sendFile(path.join(__dirname+'/src/index.html'));


})

app.route("/contact")
    .all(function(res,req, next){
        if(true){
            next();
        }
        res.sendFile(path.join(__dirname+'/src/index.html'));

    })

    .get(function(res,req){
        // affiche contacte
    })

    .post(function(res,req){
        // ajoute un contacte
        res.status(404).end()
    })


// le serv va écouté sur le port 3000
app.listen(port, () => {
    console.log(`Example app listening on port localhost:${port}`)
})


// on doit installer nodemon afin de pouvoir avoir un refresh automatique !
// on ajoute dans package.json : "start": "nodemon index.js" !
// a la place de faire node index.js pour lancé cette ligne, on fait npm run start


router.get("/", (req, res) => {
    let db = new sqlLite.Database(path.resolve("data.db"), (err) => {
     if (err) {
      console.error(err.message);
     }
     console.log("Connected to the chinook database.");
    });
    db.get(`SELECT * FROM data WHERE key = '${req.query.data}'`, (err, row) => {
     db.close((err) => {
       if (err) {
        console.error(err.message);
       }
       console.log("Close the database connection.");
     });
     res.json(row);
    });
  });